Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: DataTables
Upstream-Contact: SpryMedia, https://datatables.net/contact
Source: http://www.datatables.net/license
Files-Excluded: examples/* js/jquery.js

Files: *
Copyright: 2008-2014 SpryMedia Limited
License: MIT

Files: old-tests/unit_test.js
Copyright: 2009 Allan Jardine <allan.jardine@sprymedia.co.uk>
License: MIT

Files: build/lib/markdown.php
Copyright: 2004-2009 Michel Fortin
           2003-2006 John Gruber
License: BSD-3-clause

Files: debian/*
Copyright: 2015 Sascha Steinbiss <sascha@steinbiss.name>
License: GPL-2+

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or
 other materials provided with the distribution.
 .
 Neither the name “Markdown” nor the names of its contributors may be used to
 endorse or promote products derived from this software without specific prior
 written permission.
 .
 This software is provided by the copyright holders and contributors “as is”
 and any express or implied warranties, including, but not limited to, the
 implied warranties of merchantability and fitness for a particular purpose are
 disclaimed. In no event shall the copyright owner or contributors be liable
 for any direct, indirect, incidental, special, exemplary, or consequential
 damages (including, but not limited to, procurement of substitute goods or
 services; loss of use, data, or profits; or business interruption) however
 caused and on any theory of liability, whether in contract, strict liability,
 or tort (including negligence or otherwise) arising in any way out of the use
 of this software, even if advised of the possibility of such damage.

License: GPL-2+
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later
 version.
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
